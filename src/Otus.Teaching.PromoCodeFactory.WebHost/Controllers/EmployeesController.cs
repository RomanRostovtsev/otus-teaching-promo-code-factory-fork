﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description,
                    Id = x.Id
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        private async Task<List<Role>> GetRolesByIds(List<Guid> roleIds)
        {
            List<Role> employeeRoles = new List<Role>();
            foreach (Guid roleId in roleIds)
            {
                Role role = await _roleRepository.GetByIdAsync(roleId);
                if (role == null) throw new ArgumentException();
                employeeRoles.Add(role);
            }
            return employeeRoles;
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployee(EmployeeData employeeData) {
            List<Role> employeeRoles;
            try
            {
                employeeRoles = await GetRolesByIds(employeeData.Roles);
            }
            catch(ArgumentException)
            {
                return BadRequest();
            }

            var employee = new Employee()
            {
                Email = employeeData.Email,
                FirstName = employeeData.FirstName,
                LastName = employeeData.LastName,
                AppliedPromocodesCount = employeeData.AppliedPromocodesCount,
                Roles = new List<Role>(employeeRoles),
                Id = Guid.NewGuid()
            };
            await _employeeRepository.AddAsync(employee);
            return new EmployeeResponse()
            {
                FullName = employee.FullName,
                Email = employee.Email,
                Id = employee.Id,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description,
                    Id = x.Id
                }).ToList(),
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployee(Guid id)
        {
            try
            {
                await _employeeRepository.DeleteByIdAsync(id);
                return Ok();
            }
            catch(ArgumentException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> UpdateEmployee(Guid id, EmployeeData employeeData)
        {
            List<Role> employeeRoles;
            try
            {
                employeeRoles = await GetRolesByIds(employeeData.Roles);
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
            try
            {
                await _employeeRepository.DeleteByIdAsync(id);
            }
            catch(ArgumentException)
            {
                return NotFound();
            }
            Employee employee = new Employee()
            {
                Id = id,
                FirstName = employeeData.FirstName,
                LastName = employeeData.LastName,
                Email = employeeData.Email,
                AppliedPromocodesCount = employeeData.AppliedPromocodesCount,
                Roles = employeeRoles
            };
            await _employeeRepository.AddAsync(employee);
            return Ok();
        }

    }
}